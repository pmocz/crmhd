function AssembleFaceBFluxMatrix
% Assemble Face BFlux Matrix

Globals2D;

fluxBx = XFluxMatrix(5*Nx+1:6*Nx,:);
fluxBy = YFluxMatrix(4*Nx+1:5*Nx,:);

omegaNE = ( -fluxBx - circshift(fluxBx,Up) + fluxBy + circshift(fluxBy,Right)  ) / 4;
omegaSE = ( -fluxBx - circshift(fluxBx,Down) + circshift(fluxBy,Down) + circshift(fluxBy,Down+Right)  ) / 4;
omegaNW = ( -circshift(fluxBx,Left) - circshift(fluxBx,Up+Left) + fluxBy + circshift(fluxBy,Left)  ) / 4;

FaceBxFluxMatrix = -(omegaNE - omegaSE) / Deltay;
FaceByFluxMatrix =  (omegaNE - omegaNW) / Deltax;

end

