function mtx_out = circshift2( mtx, dir )
%rotate submatrices up/down

Globals2D;

mtx_out = mtx;

for i = 1:N_vars
    mtx_out(1+(i-1)*Nx:i*Nx,:) = circshift(mtx(1+(i-1)*Nx:i*Nx,:), dir);
end

end

