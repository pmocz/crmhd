% Driver file for solving 2D MHD eqns + advective/diffusive CR pressure
% setup: MHD shock (upstream only) with CR pressure
% Philip Mocz
% Astronomy 253 Plasma Astrophysics, Spring 2014
% Harvard University

clear all;
close all;
clc;
format long;
addpath('../FVCT_2D/');
addpath('../FVCT_2D/Plotting');

Globals2D;


%% setup
BoxSizeX                 = 1.0;       % box size in x direction
BoxSizeY                 = 1.0;       % box size in y direction
Nx                       = 256;       % number of cells in x direction
Ny                       = 256;       % number of cells in y direction
courant_fac              = 0.4;       % courant factor
t_final                  = 0.04;      % final time of simulation
t_output                 = 0.0001;    % output frequency
Gamma                    = 5/3.;      % gas gamma
initial_condition_string = 'MHDshock_half';

% shock
sim_case = 'M100upstream';
switch sim_case
    case 'M100upstream'
        rho_L     = 1;
        P_L       = 0.6;
        vx_L      = 100;
        vy_L      = 0;
        Bx_shock  = 0;
        By_shock  = 0.1;
end

% level of density variation (chosen so rms is ~0.2)
beta_L = 0.0073;0.007;

% cosmic ray pressure
eta = 0.6;    % efficiency:  Pc = eta*rho_L*vx_L^2;
D = 0;      % diffusion coefficient  --  not used


%% generate mesh
fprintf('generating mesh ...\n');
Deltax = BoxSizeX/Nx;
Deltay = BoxSizeY/Ny;
Vol = Deltax*Deltay;
[X,Y] = meshgrid( Deltax*((1:Nx)-0.5), Deltay*((1:Ny)-0.5) );
X = X';
Y = Y';


%% set initial conditions
fprintf('setting ICs ...\n');
time = 0;
rng(47); % seed random number generator
N_k = 100;
A = rand(N_k,N_k);
phi =rand(N_k,N_k)*2*pi;
xi = 0;
[init_solution_coeffs, faceBx, faceBy] = InitializeSolutionCoeffs(initial_condition_string,A,phi,xi,X,Y,0);
solution_coeffs = init_solution_coeffs;
filename = '../../output/snap_000.mat';
save(filename,'BoxSizeX','BoxSizeY','Nx','Ny','solution_coeffs','faceBx','faceBy','time','Gamma');

% plot the initial condition
fprintf('plotting ICs ...\n');
output_count = 1;
[rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs);
plot_solution2D(log10(rho),'$\log_{10}(\rho)$',[-1.2 1],time,1,'B');
plot_solution2D(Pc,'$P_c$',[0 eta*rho_L*vx_L^2],time,2,'B');
plot_solution2D(log10(sqrt(Bx.^2+By.^2)),'$\log_{10}(B)$',[-3 1],time,3,'B');
drawnow


%% main loop
tic
% integrate in time
fprintf('time integration ...\n');
while time < t_final
    % get conserved variables
    Q = solution_coeffs * Vol;
    % get primitive variables from U
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs);
    
    % get global time step from CFL condition
    c0 = sqrt( Gamma*(P(:)-0.5*(Bx(:).^2+By(:).^2)) ./ rho(:) );
    ca = sqrt( (Bx(:).^2 + By(:).^2) ./ rho(:) );
    cf = sqrt( 0.5*(c0.^2+ca.^2) + 0.5*sqrt((c0.^2+ca.^2).^2) );
    delta_t = courant_fac * min( sqrt(Vol/pi)./(cf+sqrt((vx(:)).^2+(vy(:)).^2)) );
    output_this_turn = 0;
    if time + delta_t > output_count*t_output
        delta_t = output_count*t_output - time;
        output_this_turn = 1;
    end
    
    % calculate gradients
    grad_rho = GetGradients(rho);
    grad_vx  = GetGradients(vx);
    grad_vy  = GetGradients(vy);
    grad_P   = GetGradients(P);
    grad_Bx  = GetGradients(Bx);
    grad_By  = GetGradients(By);
    grad_Bx{1} = ( faceBx - circshift(faceBx,Left) ) / Deltax;
    grad_By{2} = ( faceBy - circshift(faceBy,Down) ) / Deltay;
    grad_Pc  = GetGradients(Pc);
    
    % apply slope limiting
    grad_rho = SlopeLimit(rho,grad_rho);
    grad_vx  = SlopeLimit(vx,grad_vx);
    grad_vy  = SlopeLimit(vy,grad_vy);
    grad_P   = SlopeLimit(P,grad_P);
    grad_Bx  = SlopeLimit(Bx,grad_Bx);
    grad_By  = SlopeLimit(By,grad_By);
    %grad_Pc  = SlopeLimit(Pc,grad_Pc);
    
    % compute flux across cells
    AssembleFluxMatrix(grad_rho,grad_vx,grad_vy,grad_P,grad_Bx,grad_By,grad_Pc,rho,vx,vy,P,Bx,By,delta_t);
    AssembleFaceBFluxMatrix;
    
    % update solution
    Q = Q - delta_t * Deltay * XFluxMatrix;                   % flux thru right face
    Q = Q + delta_t * Deltay * circshift2(XFluxMatrix,Left);  % flux thru left face
    Q = Q - delta_t * Deltax * YFluxMatrix;                   % flux thru above face
    Q = Q + delta_t * Deltax * circshift2(YFluxMatrix,Down);  % flux thru below face
    Q = Q + delta_t * Deltax * Deltay * SourceMatrix;         % source terms
    solution_coeffs = Q / Vol;
    faceBx = faceBx + delta_t * FaceBxFluxMatrix;
    faceBy = faceBy + delta_t * FaceByFluxMatrix;
    solution_coeffs(4*Nx+1:5*Nx,:) = 0.5*( faceBx + circshift(faceBx,Left) );
    solution_coeffs(5*Nx+1:6*Nx,:) = 0.5*( faceBy + circshift(faceBy,Down) );
    
    % advance time
    time = time + delta_t;
    fprintf('t = %f out of %f.\n',time,t_final);
    
    % Set boundary conditions -- left/right edges
    % [x=0] inflow
    [L_solution_coeffs, ~, ~] = InitializeSolutionCoeffs(initial_condition_string,A,phi,xi,X(1:2,:),Y(1:2,:),time);
    solution_coeffs(1+(0:6)*Nx,:) = L_solution_coeffs([1 3 5 7 9 11 13],:);
    solution_coeffs(2+(0:6)*Nx,:) = L_solution_coeffs([2 4 6 8 10 12 14],:);
    % [x=BoxSizeX] outflow
    solution_coeffs(Nx+(0:6)*Nx,:)   = solution_coeffs(Nx-2+(0:6)*Nx,:);  % outflow, 0 gradient
    solution_coeffs(Nx-1+(0:6)*Nx,:) = solution_coeffs(Nx-2+(0:6)*Nx,:);
    
    % plot the solution at time intervals
    if output_this_turn == 1
        filename = ['../../output/snap_' sprintf('%3.3d',output_count) '.mat'];
        save(filename,'BoxSizeX','BoxSizeY','Nx','Ny','solution_coeffs','faceBx','faceBy','time','Gamma');
        clf;
        [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs);
        plot_solution2D(log10(rho),'$\log_{10}(\rho)$',[-1.2 1],time,1,'B');
        plot_solution2D(Pc,'$P_c$',[0 eta*rho_L*vx_L^2],time,2,'B');
        plot_solution2D(log10(sqrt(Bx.^2+By.^2)),'$\log_{10}(B)$',[-3 1],time,3,'B');
        drawnow
        output_count = output_count + 1;
    end
end
toc
