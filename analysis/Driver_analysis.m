% Analysis of output script
% Philip Mocz
% Astronomy 253 Plasma Astrophysics, Spring 2014
% Harvard University

clear all;
close all;
clc;
format long;
addpath('../FVCT_2D/');
addpath('../FVCT_2D/Plotting');

Globals2D;
analyze_sim = 2;1;2;3;
switch analyze_sim
    case 1
        ofolder = 'Bperp512';
        mname1 = 'M100Bperp_rho';
        mname2 = 'M100Bperp_B';
        Nsim = 158;
        B_init = 0.001;
    case 2
        ofolder = 'Bperp512mod';
        mname1 = 'M100Bperpmod_rho';
        mname2 = 'M100Bperpmod_B';
        Nsim = 204;
        B_init = 0.001;
    case 3
        ofolder = 'Bperp256up';
        mname1 = 'upM100Bperp_rho';
        mname2 = 'upM100Bperp_B';
        Nsim = 401;
        B_init = 0.1;
end

%% load sims
fprintf('loading snapshots ...\n');
tfinal = Nsim*0.01;
cell_solution_coeffs = cell(Nsim,1);
for i = 1:Nsim
    filename = ['../../output/', ofolder, '/snap_', sprintf('%3.3d',i-1), '.mat'];
    %filename = ['../../output/snap_', sprintf('%3.3d',i-1), '.mat'];
    load(filename);
    cell_solution_coeffs{i} = solution_coeffs;
end


% generate mesh
fprintf('generating mesh ...\n');
Deltax = BoxSizeX/Nx;
Deltay = BoxSizeY/Ny;
Vol = Deltax*Deltay;
[X,Y] = meshgrid( Deltax*((1:Nx)-0.5), Deltay*((1:Ny)-0.5) );
X = X';
Y = Y';


%% plot
fig_num = 1;
for i = 0:50:150
    solution_coeffs = cell_solution_coeffs{i+1};
    time = i/100;
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs);
    plot_solution2D(rho,'$\rho$',[0 10],time,fig_num,'B'); fig_num = fig_num+1;
    plot_solution2D(sqrt(Bx.^2+By.^2),'$B$',[0 0.05],time,fig_num,'B'); fig_num = fig_num+1;
    plot_solution2D(Pc,'$P_{\rm c}$',[0 0.6+0.2],time,fig_num,'B') ;fig_num = fig_num+1;
    drawnow
    
    %figure;
    %quiver(X,Y,Bx,By)
    %hold on
    %plot([1 1],[0 1],'r','linewidth',2);
    %hold off
    %axis([0.9 1.1 0.4 0.6])
end


%% plot more
plot_solution2D(vx,'$v_x$',[-1.4 1.4],time,fig_num,'B'); fig_num = fig_num+1;
colormap('jet')
%sqrt(vx.^2+vy.^2)

%% analysis -- amplification
time = linspace(0,tfinal,Nsim);
B_avg =  zeros(Nsim,1); B_max = B_avg;
cut = 1:Ny;
for i = 1:Nsim
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(cell_solution_coeffs{i});
    Brho = sqrt(Bx(cut,:).^2+By(cut,:).^2) .* rho(cut,:);
    B_avg(i) = mean(Brho(:)) / mean(mean(rho(cut,:)));
    B_max(i) = max(max(sqrt(Bx(cut,:).^2+By(cut,:).^2)));
end

semilogy(time,B_avg/B_init,'b','linewidth',2)
hold on
semilogy(time,B_max/B_init,'r','linewidth',2)
hold off
title('Magnetic Field Amplification','fontsize',14,'interpreter','latex');
xlabel('$t$','fontsize',12,'interpreter','latex')
ylabel('Amplification','fontsize',12,'interpreter','latex')
lh = legend('avg','max');
set(lh,'interpreter','latex')
axis([0 1.6 1 100])


%% analysis -- power spectrum
figure;
colors = {'r','g', 'b', 'k'};

c=1;
for i = 1+(0:50:150)
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(cell_solution_coeffs{i});
    B = sqrt(Bx.^2+By.^2);
    [Pf, k, total_power] = radial_power_spectrum(rho(cut,:),BoxSizeY);
    loglog(k/(2*pi), Pf, colors{c},'linewidth',2); c = c+1;
    hold on
end
title('Density Power Spectra','fontsize',14,'interpreter','latex');
xlabel('$k$','fontsize',12,'interpreter','latex')
ylabel('$P_k$','fontsize',12,'interpreter','latex')
lh = legend('$t=0$','$t=0.5$','$t=1.0$','$t=1.5$');
set(lh,'interpreter','latex')
axis([1 132, 10^4, 10^14 ]);

figure;
c=1;
for i = 1+(0:50:150)
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(cell_solution_coeffs{i});
    Benergy = (Bx.^2+By.^2)/2;
    [Pf, k, total_power] = radial_power_spectrum(Benergy(cut,:),BoxSizeY);
    loglog(k/(2*pi), Pf, colors{c},'linewidth',2); c = c+1;
    hold on
end
title('Magnetic Energy Density Power Spectra','fontsize',14,'interpreter','latex');
xlabel('$k$','fontsize',12,'interpreter','latex')
ylabel('$P_k$','fontsize',12,'interpreter','latex')
lh = legend('$t=0$','$t=0.5$','$t=1.0$','$t=1.5$');
set(lh,'interpreter','latex')
axis([1 132, 10^-5, 10^2 ]);


%% analysis -- density pdf
figure;
colors = {'r','g', 'b', 'k'};

c=1;
for i = 1+(0:50:150)
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(cell_solution_coeffs{i});
    B = sqrt(Bx.^2+By.^2);
    rho_cut = rho(cut,:);
    [pdf_rho,rho_i] = ksdensity(rho_cut(:));
    loglog(rho_i,pdf_rho,'linewidth',2,'color',colors{c}); c = c+1;
    hold on
end
title('Density PDF','fontsize',14,'interpreter','latex');
xlabel('$\rho$','fontsize',12,'interpreter','latex')
ylabel('${\rm PDF}(\rho)$','fontsize',12,'interpreter','latex')
lh = legend('$t=0$','$t=0.5$','$t=1.0$','$t=1.5$');
set(lh,'interpreter','latex')
axis([10^-1 10^1, 10^-4 10^1 ]);
hold off;


%% make movie 1
vidObj = VideoWriter(['../../movie/' mname1 '.avi']);
vidObj.Quality = 100;
vidObj.FrameRate = 16;
open(vidObj);
for i = 1:Nsim;
    solution_coeffs = cell_solution_coeffs{i};
    time = (i-1)/100;
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs);
    fig_handle = figure(1);
    switch analyze_sim
        case 1
            plot_solution2D(rho,'$\rho$',[0 10],time,1,'B');
        case 2
            plot_solution2D(rho,'$\rho$',[0 10],time,1,'B');
        case 3
            plot_solution2D(log10(rho),'$\log_{10}(\rho)$',[-1.2 1],time,1,'B');
    end
    drawnow;
    pause(0.01);
    writeVideo(vidObj, getframe(fig_handle));
    pause(0.01);
    clf;
end

close(gcf)

%# save as AVI file, and open it using system video player
close(vidObj);


%% make movie 2
vidObj = VideoWriter(['../../movie/' mname2 '.avi']);
vidObj.Quality = 100;
vidObj.FrameRate = 16;
open(vidObj);
for i = 1:Nsim;
    solution_coeffs = cell_solution_coeffs{i};
    time = (i-1)/100;
    [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs);
    fig_handle = figure(1);
    switch analyze_sim
        case 1
            plot_solution2D(sqrt(Bx.^2+By.^2),'$B$',[0 0.05],time,1,'B');
        case 2
            plot_solution2D(sqrt(Bx.^2+By.^2),'$B$',[0 0.05],time,1,'B');
        case 3
            time = time/100;
            plot_solution2D(log10(sqrt(Bx.^2+By.^2)),'$\log_{10}(B)$',[-3 1],time,1,'B');
    end
    drawnow;
    pause(0.01);
    writeVideo(vidObj, getframe(fig_handle));
    pause(0.01);
    clf;
end

close(gcf)

%# save as AVI file, and open it using system video player
close(vidObj);

