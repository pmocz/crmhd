function fluxes = Riemann_Rusanov(rho_prime,vx_prime,vy_prime,P_prime,Bx_prime,By_prime,dir)
% compute Rusanov Flux
% dir = 1, 2 for x,y
% Pc is solved via implicit Crank-Nicolson elsewhere, its fluxes here are 0

Globals2D;

q1_half = cell(2,1);
q2_half = cell(2,1);
q3_half = cell(2,1);
q4_half = cell(2,1);
q5_half = cell(2,1);
q6_half = cell(2,1);
c0 = cell(2,1);
cax = cell(2,1);
ca = cell(2,1);
cf = cell(2,1);
vn = cell(2,1);

for i = 1:2
    q1_half{i} = rho_prime{i};
    q2_half{i} = rho_prime{i}.*vx_prime{i};
    q3_half{i} = rho_prime{i}.*vy_prime{i};
    q4_half{i} = (P_prime{i}-0.5*(Bx_prime{i}.^2+By_prime{i}.^2))/(Gamma-1)...
        + 0.5*rho_prime{i}.*(vx_prime{i}.^2+vy_prime{i}.^2)...
        + 0.5*(Bx_prime{i}.^2+By_prime{i}.^2);
    q5_half{i} = Bx_prime{i};
    q6_half{i} = By_prime{i};
end

switch dir
    case 1
        nbr = Right;
    case 2
        nbr = Up;
end

% averaged flux
q1_star = 0.5*( q1_half{2} + circshift(q1_half{1},nbr) );
q2_star = 0.5*( q2_half{2} + circshift(q2_half{1},nbr) );
q3_star = 0.5*( q3_half{2} + circshift(q3_half{1},nbr) );
q4_star = 0.5*( q4_half{2} + circshift(q4_half{1},nbr) );
q5_star = 0.5*( q5_half{2} + circshift(q5_half{1},nbr) );
q6_star = 0.5*( q6_half{2} + circshift(q6_half{1},nbr) );

% impose continuous normal B field
switch dir
    case 1
        q5_half{2} = q5_star;
        Bx_prime{2} = q5_half{2};
        q5_half{1} = circshift(q5_star,Left);
        Bx_prime{1} = q5_half{1};
    case 2
        q6_half{2} = q6_star;
        By_prime{2} = q6_half{2};
        q6_half{1} = circshift(q6_star,Down);
        By_prime{1} = q6_half{1};
end

P_star = (Gamma-1)*(q4_star-0.5*(q2_star.^2+q3_star.^2)./q1_star-0.5*(q5_star.^2+q6_star.^2)) + 0.5*(q5_star.^2+q6_star.^2);

switch dir
    case 1
        fluxes = [q2_star; ...
            q2_star.^2./q1_star+P_star - q5_star.^2; ...
            q2_star.*q3_star./q1_star - q5_star.*q6_star; ...
            (q4_star+P_star).*q2_star./q1_star - q5_star.*(q5_star.*q2_star+q6_star.*q3_star)./q1_star; ...
            0*q1_star; ...
            q2_star./q1_star.*q6_star - q3_star./q1_star.*q5_star;
            0*q1_star];
    case 2
        fluxes = [q3_star; ...
            q3_star.*q2_star./q1_star - q6_star.*q5_star; ...
            q3_star.^2./q1_star+P_star - q6_star.^2; ...
            (q4_star+P_star).*q3_star./q1_star - q6_star.*(q5_star.*q2_star+q6_star.*q3_star)./q1_star; ...
            q3_star./q1_star.*q5_star - q2_star./q1_star.*q6_star; ...
            0*q1_star;
            0*q1_star];
end

% Local Lax-Friedrich's/Rusanov contribution
for i = 1:2
    % wave speeds
    c0{i}  = sqrt( Gamma*(P_prime{i}-0.5*(Bx_prime{i}.^2+By_prime{i}.^2)) ./ rho_prime{i} );
    cax{i} = sqrt( ((dir == 1)*Bx_prime{i} + (dir == 2)*By_prime{i}).^2 ./ rho_prime{i} );
    ca{i}  = sqrt( (Bx_prime{i}.^2+By_prime{i}.^2) ./ rho_prime{i} );
    cf{i}  = sqrt( 0.5*(c0{i}.^2+ca{i}.^2) + 0.5*sqrt((c0{i}.^2+ca{i}.^2).^2-4*c0{i}.^2.*cax{i}.^2) );
    vn{i}  = (dir == 1)*vx_prime{i} + (dir == 2)*vy_prime{i};
end

C = max(  max( abs(vn{2} + cf{2}), abs(vn{2} - cf{2}) ),...
    max( abs(circshift(vn{1},nbr) + circshift(cf{1},nbr)), abs(circshift(vn{1},nbr) - circshift(cf{1},nbr)) )  ); % local,max wave speed
C = repmat(C,N_vars,1);
fluxes = fluxes + 0.5*C.*[q1_half{2} - circshift(q1_half{1},nbr);...
    q2_half{2} - circshift(q2_half{1},nbr);...
    q3_half{2} - circshift(q3_half{1},nbr);...
    q4_half{2} - circshift(q4_half{1},nbr);...
    q5_half{2} - circshift(q5_half{1},nbr);...
    q6_half{2} - circshift(q6_half{1},nbr);...
    zeros(size(q1_half{1}));...
    ];

end