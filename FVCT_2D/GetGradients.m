function [grad_var] = GetGradients(var)
% get gradients of primitive variables at each cell

Globals2D;

grad_var = cell(2,1);

grad_var{1} = ( circshift(var,Right) - circshift(var,Left) ) / (2*Deltax);
grad_var{2} = ( circshift(var,Up)    - circshift(var,Down) ) / (2*Deltay);

end