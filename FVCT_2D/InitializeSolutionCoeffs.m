function [solution_coeffs, faceBx, faceBy] = InitializeSolutionCoeffs(initial_condition_string,A,phi,xi,Xc,Yc,time)
% initialize the solution coefficients
Globals2D;

nx = size(Xc,1);
ny = size(Yc,2);

switch initial_condition_string
    
    case 'OrszagTang'
        init_q1 = @(x1,x2) Gamma^2/(4*pi)*ones(nx,ny);  %rho
        init_q2 = @(x1,x2) Gamma^2/(4*pi)*(-sin(2*pi*x2));  %rho v_x
        init_q3 = @(x1,x2) Gamma^2/(4*pi)*( sin(2*pi*x1));  %rho v_y
        init_q4 = @(x1,x2) Gamma/(4*pi*(Gamma-1)) ...
            + 0.5*Gamma^2/(4*pi)*(-sin(2*pi*x2).^2+sin(2*pi*x1).^2) ...
            + 0.5*(sin(2*pi*x2).^2+sin(4*pi*x1).^2)/(4*pi);  %rho e
        init_q5 = @(x1,x2) -sin(2*pi*x2)/sqrt(4*pi);  %Bx
        init_q6 = @(x1,x2)  sin(4*pi*x1)/sqrt(4*pi);  %By
        init_Az = @(x1,x2) cos(4*pi*x1)/(4*pi*sqrt(4*pi)) + cos(2*pi*x2)/(2*pi*sqrt(4*pi));
        init_q7 = @(x1,x2) zeros(nx,ny); %Pc
        
    case 'MHDshock'
        init_q1 = @(x1,x2) (rho_L*(x1<=0.5*BoxSizeX)+rho_R*(x1>0.5*BoxSizeX)).*mod_density(x1,x2,A,phi,xi,time);  %rho
        init_q2 = @(x1,x2) (rho_L*vx_L*(x1<=0.5*BoxSizeX)+rho_R*vx_R*(x1>0.5*BoxSizeX)).*mod_density(x1,x2,A,phi,xi,time);  %rho v_x
        init_q3 = @(x1,x2) (rho_L*vy_L*(x1<=0.5*BoxSizeX)+rho_R*vy_R*(x1>0.5*BoxSizeX)).*mod_density(x1,x2,A,phi,xi,time);  %rho v_y
        init_q4 = @(x1,x2) (P_L/(Gamma-1)+0.5*rho_L*mod_density(x1,x2,A,phi,xi,time)*(vx_L^2+vy_L^2)).*(x1<=0.5*BoxSizeX)+(P_R/(Gamma-1)+0.5*rho_R*mod_density(x1,x2,A,phi,xi,time)*(vx_R^2+vy_R^2)).*(x1>0.5*BoxSizeX);  %rho e
        init_q5 = @(x1,x2) Bx_shock*ones(nx,ny);  %Bx
        init_q6 = @(x1,x2) By_shock*ones(nx,ny);  %By
        init_Az = @(x1,x2) -By_shock*x1 + Bx_shock*x2; % magnetic potential, used to set ICs in divergence free way
        init_q7 = @(x1,x2) zeros(nx,ny); %Pc
        
    case 'MHDshock_half'
        init_q1 = @(x1,x2) rho_L*mod_density_half(x1,x2,A,phi,time);  %rho
        init_q2 = @(x1,x2) rho_L*mod_density_half(x1,x2,A,phi,time)*vx_L;  %rho v_x
        init_q3 = @(x1,x2) rho_L*mod_density_half(x1,x2,A,phi,time)*vy_L;  %rho v_y
        init_q4 = @(x1,x2) P_L/(Gamma-1)+0.5*rho_L*mod_density_half(x1,x2,A,phi,time)*(vx_L^2+vy_L^2);  %rho e
        init_q5 = @(x1,x2) Bx_shock*ones(nx,ny);  %Bx
        init_q6 = @(x1,x2) By_shock*ones(nx,ny);  %By
        init_Az = @(x1,x2) -By_shock*x1 + Bx_shock*x2; % magnetic potential, used to set ICs in divergence free way
        init_q7 = @(x1,x2) eta * rho_L * vx_L^2 * x1 / BoxSizeX; %Pc
        
    otherwise
        error('invalid initial_condition_string')
end


solution_coeffs = [init_q1(Xc,Yc); ...
    init_q2(Xc,Yc); ...
    init_q3(Xc,Yc); ...
    init_q4(Xc,Yc)];

faceBx =  ( init_Az(Xc+Deltax/2,Yc+Deltay/2) - init_Az(Xc+Deltax/2,Yc-Deltay/2) ) / Deltay;
faceBy = -( init_Az(Xc+Deltax/2,Yc+Deltay/2) - init_Az(Xc-Deltax/2,Yc+Deltay/2) ) / Deltax;

solution_coeffs = [ solution_coeffs; ...
    0.5*( faceBx + circshift(faceBx,Left) ); ...
    0.5*( faceBy + circshift(faceBy,Down) ); ...
    init_q7(Xc,Yc)];

% print rms of density
if time == 0
    rho = solution_coeffs(1:nx,1:ny);
    rms_L = rho(Xc<=0.5*BoxSizeX);
    rms_R = rho(Xc>0.5*BoxSizeX);
    rms_L = sqrt(sum((rms_L-mean(rms_L)).^2)/length(rms_L));
    rms_R = sqrt(sum((rms_R-mean(rms_R)).^2)/length(rms_R));
    fprintf('LHS rms =  %f.\n',rms_L);
    fprintf('RHS rms =  %f.\n',rms_R);
end

end

% make density have log normal pdf
function f = mod_density(x,y,A,phi,xi,time)
global vx_L;
global beta_L;
global beta_R;
global BoxSizeX;
global BoxSizeY;
global k_min;
global k_max;
% f = 0;
% for kx = 10:50
%     for ky = 10:50
%         f = f + A(kx,ky)*sin(kx*(x-time*vx_L)+phi(kx,ky)).* ...
%             sin(ky*y+xi(kx,ky));
%     end
% end
f = 0;
for kx = k_min:k_max
    for ky = k_min:k_max
        f = f + A(kx-k_min+1,ky-k_min+1)*sin(2*pi/BoxSizeY*(kx*(x-time*vx_L)+ky*y)+phi(kx-k_min+1,ky-k_min+1));
    end
end
f = exp(-(beta_L*(x<=0.5*BoxSizeX) + beta_R*(x>0.5*BoxSizeX)).*f);
end

% make density have log normal pdf
function f = mod_density_half(x,y,A,phi,time)
global vx_L;
global beta_L;
global BoxSizeY;
f = 0;
for kx = -32:32
    for ky = -32:32%-4:4
        f = f + A(kx+33,ky+33)*sin(2*pi/BoxSizeY*(kx*(x-time*vx_L)+ky*y)+phi(kx+33,ky+33));
    end
end
f = exp(-beta_L.*f);
end