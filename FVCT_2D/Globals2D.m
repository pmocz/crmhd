% Purpose: declare global variables

global N_vars               % number of fluid variables
global Gamma                % gamma of gas
global BoxSizeX             % length of domain in x direction
global BoxSizeY             % length of domain in y direction
global Nx                   % number of cells in x direction
global Ny                   % number of cells in y direction
global Deltax               % x sidelength of cell
global Deltay               % y sidelength of cell
global Vol                  % volume of cell
global X                    % cell centers x
global Y                    % cell centers y
global XFluxMatrix          % flux between shared faces (left,right) matrix
global YFluxMatrix          % flux between shared faces (above,below) matrix
global FaceBxFluxMatrix     % flux for Bx between shared faces matrix
global FaceByFluxMatrix     % flux for By between shared faces matrix
global SourceMatrix         % source terms

N_vars = 7;          % # of variables to update U = (rho, rho v_x, rho v_y, rho e, Bx, By)

% circshift directions -- tells you the direction of neighbor
global Up;
global Down;
global Left;
global Right;

Up    = [  0 -1 ];
Down  = [  0  1 ];
Left  = [  1  0 ];
Right = [ -1  0 ];


% shock
global rho_L;
global P_L;
global vx_L;
global vy_L;
global rho_R;
global P_R;
global vx_R;
global vy_R;
global Bx_shock;
global By_shock;

% level of density variation
global beta_L;
global beta_R;

% CR pressure
global eta;
global D;

% IC k range
global k_min;
global k_max;
