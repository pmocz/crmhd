function [rho,vx,vy,P,Bx,By,Pc] = GetPrimitive(solution_coeffs)
% get primitive variables at each cell

Globals2D;

rho = solution_coeffs(1:Nx,:);
vx = solution_coeffs(1+Nx:2*Nx,:)./rho;
vy = solution_coeffs(2*Nx+1:3*Nx,:)./rho;
Bx = solution_coeffs(4*Nx+1:5*Nx,:);
By = solution_coeffs(5*Nx+1:6*Nx,:);
rho_u = solution_coeffs(3*Nx+1:4*Nx,:) - 0.5*rho.*(vx.^2+vy.^2) - 0.5*(Bx.^2+By.^2);
P = (Gamma-1)*rho_u+0.5*(Bx.^2+By.^2); % P = total
Pc = solution_coeffs(6*Nx+1:7*Nx,:);

end